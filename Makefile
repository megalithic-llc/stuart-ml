.PHONY: lint
lint:
	luacheck --codes src spec spec-redis examples

.PHONY: check
check:
	busted -v --defer-print
